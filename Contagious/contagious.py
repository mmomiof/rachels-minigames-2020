import pygame, sys, random, math
from pygame.locals import *

pygame.init()
fps = pygame.time.Clock()

SCREEN_WIDTH = 1280
SCREEN_HEIGHT = 720

window = pygame.display.set_mode( ( SCREEN_WIDTH, SCREEN_HEIGHT ) )
pygame.display.set_caption( "Contagious" )

colorBackground = pygame.Color( 255, 255, 255 )
colorRadius = pygame.Color( 255, 0, 0, 100 )

imageBrick = pygame.image.load( "assets/brick.png" )
imagePerson = pygame.image.load( "assets/person.png" )
imageRadius = pygame.image.load( "assets/radius.png" )

class Thing:
    def __init__( self, n, s, r, br, i ):
        self.rectangle = r
        self.surface = s
        self.blitRect = br
        self.isInfected = False
        self.moveCounter = 0
        self.moveDirection = None
        self.speed = 5
        self.isPlayer = False
        self.isInanimate = i
        self.name = n
        
    def MakePlayer( self ):
        self.speed = 5
        self.isPlayer = True
        self.Infect( "life" )
        
    def Infect( self, fromWho ):
        if ( self.isInfected ): return
        print( "Person " + self.name + " infected by " + fromWho )
        self.isInfected = True
        self.blitRect = Rect( 40, 0, 40, 40 )
        
    def AIMove( self ):
        if ( self.moveCounter == 0 ):
            self.moveCounter = random.randint( 10, 50 )
            self.moveDirection = random.randint( 1, 4 )
        else:
            self.moveCounter -= 1
            self.Move( self.moveDirection )
        
    def PlayerMove( self ):
        keys = pygame.key.get_pressed()
        if ( keys[ K_UP ] or keys[ K_w ] ):
            self.Move( 1 )
        elif ( keys[ K_DOWN ] or keys[ K_s ] ):
            self.Move( 2 )
        if ( keys[ K_LEFT ] or keys[ K_a ] ):
            self.Move( 3 )
        elif ( keys[ K_RIGHT ] or keys[ K_d ] ):
            self.Move( 4 )
    
    def Move( self, direction ):
        if ( direction == 1 ): # Up
            self.rectangle.y -= self.speed
        if ( direction == 2 ): # Down
            self.rectangle.y += self.speed
        if ( direction == 3 ): # Left
            self.rectangle.x -= self.speed
        if ( direction == 4 ): # Right
            self.rectangle.x += self.speed
        
        # Keep in game screen
        if ( self.isPlayer ):
            if ( self.rectangle.x < 0 ): self.rectangle.x = 0
            elif ( self.rectangle.x > SCREEN_WIDTH-40 ): self.rectangle.x = SCREEN_WIDTH-40
            if ( self.rectangle.y < 0 ): self.rectangle.y = 0
            elif ( self.rectangle.y > SCREEN_HEIGHT-40 ): self.rectangle.y = SCREEN_HEIGHT-40
        else:
            if ( self.rectangle.x < 120 ): self.rectangle.x = 120
            elif ( self.rectangle.x > SCREEN_WIDTH-40 ): self.rectangle.x = SCREEN_WIDTH-40
            if ( self.rectangle.y < 80 ): self.rectangle.y = 80
            elif ( self.rectangle.y > SCREEN_HEIGHT-40 ): self.rectangle.y = SCREEN_HEIGHT-40
    
    def GetCenterRect( self ):
        center = Rect( self.rectangle.x + self.rectangle.width/2, self.rectangle.y + self.rectangle.height/2, self.rectangle.width, self.rectangle.height )
        return center
        
    def Draw( self, window ):
        # Draw radius
        if ( self.isInanimate == False ):
            centerX = self.rectangle.x + self.rectangle.width / 2 - 80/2
            centerY = self.rectangle.y + self.rectangle.height / 2 - 80/2
            window.blit( imageRadius, ( centerX, centerY ) )
        
        # Draw sprite
        if ( self.surface is not None ):
            window.blit( self.surface, self.rectangle, self.blitRect )
            

def GetDistance( rect1, rect2 ):
    xDiff = rect1.x - rect2.x
    yDiff = rect1.y - rect2.y
    
    return math.sqrt( xDiff * xDiff + yDiff * yDiff )

def IsCollision( rect1, rect2 ):
    # Bounding box
    pass

house = [
    Thing( "brick", imageBrick, Rect( 0, 0, 40, 40 ), Rect( 0, 0, 40, 40 ), True ),
    Thing( "brick", imageBrick, Rect( 40, 0, 40, 40 ), Rect( 0, 0, 40, 40 ), True ),
    Thing( "brick", imageBrick, Rect( 80, 0, 40, 40 ), Rect( 0, 0, 40, 40 ), True ),
    Thing( "brick", imageBrick, Rect( 0, 40, 40, 40 ), Rect( 0, 0, 40, 40 ), True ),
    Thing( "brick", imageBrick, Rect( 80, 40, 40, 40 ), Rect( 0, 0, 40, 40 ), True )
]

people = []
TOTAL_PEOPLE = 40

for i in range( 1, TOTAL_PEOPLE ):
    x = random.randint( 120, SCREEN_WIDTH )
    y = random.randint( 0, SCREEN_HEIGHT )
    people.append( Thing( "Person" + str( i ), imagePerson, Rect( x, y, 40, 40 ),  Rect( 0, 0, 40, 40 ), False ) )

player = Thing( "player", imagePerson, Rect( 40, 40, 40, 40 ), Rect( 0, 0, 40, 40 ), False )
player.MakePlayer()

while True:
    window.fill( colorBackground )
    
    for event in pygame.event.get():
        if ( event.type == QUIT ):
            pygame.quit()
            sys.exit()
            
    for brick in house:
        brick.Draw( window )
            
    for person in people:
        # Move around
        person.AIMove()
        # Draw
        person.Draw( window )
    
    player.PlayerMove()
    player.Draw( window )
    
    # Compare distances
    for personA in people:
        for personB in people:
            if ( personA is personB ): continue
            
            dist = GetDistance( personA.GetCenterRect(), personB.GetCenterRect() )
            
            if ( dist <= 40 ):
                if ( personA.isInfected ):
                    personB.Infect( personA.name )
                elif ( personB.isInfected ):
                    personA.Infect( personB.name )
            
        # And check with the player
        dist = GetDistance( personA.GetCenterRect(), player.GetCenterRect() )
        if ( dist <= 40 ):
            if ( personA.isInfected ):
                player.Infect( personA.name )
            elif ( player.isInfected ):
                personA.Infect( player.name )
        
    pygame.display.update()
    fps.tick( 30 )
