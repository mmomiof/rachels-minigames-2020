#!/usr/bin/python
# -*- coding: utf-8 -*-

import random
import math

import pygame, sys
from pygame.locals import *

from Ezha import BaseState
from Ezha import FontManager
from Ezha import ImageManager
from Ezha import LanguageManager
from Ezha import SoundManager
from Ezha import Button
from Ezha import Image
from Ezha import Label

class TitleState( BaseState ):
    def __init__( self ):
        self.stateName = "titlestate"
        self.backgroundColor = pygame.Color( 151, 255, 140 )
        self.scoreColor = pygame.Color( 255, 255, 255 )


    def GotoState( self ):
        return self.gotoState
        
    def Setup( self, screenWidth, screenHeight ):
        self.screenWidth = screenWidth
        self.screenHeight = screenHeight

        ImageManager.Add( "background",     "assets/images/title-screen.png" )
        ImageManager.Add( "button",         "assets/images/button.png" )
        ImageManager.Add( "title",          "assets/images/title-text.png" )

        self.animateCounter = 0;
        self.zoomCounter = 1;
        
        self.gotoState = ""
        
        self.buttonItems = {}
        
        self.imageItems = {}
        
        self.labelItems = {}
        
        self.imageItems["title"] = Image()
        self.imageItems["title"].Setup( { 
            "image" : ImageManager.Get( "title" ),
            "imagePosition" : ( 0, 0 ),
            "imageBlitRect" : ( 0, 0, 1280, 720 ),
            "effect" : "bob"
            } )
            
        self.buttonItems["playButton"] = Button()
        self.buttonItems["playButton"].Setup( { 
            "backgroundImage" : ImageManager.Get( "button" ),
            "backgroundPosition" : ( 1280*.25, 550 ),
            #"iconImage" : self.images["button_icons"],
            #"iconPosition" : ( 40, 25 ),
            #"iconBlitRect" : ( 150, 0, 50, 50 ),
            "text" : "Play",
            "textPosition" : ( 110, 15 ),
            "textColor" : pygame.Color( 75, 90, 5 ),
            "font" : FontManager.Get( "main" )
            } )
        
        self.buttonItems["aboutButton"] = Button()
        self.buttonItems["aboutButton"].Setup( { 
            "backgroundImage" : ImageManager.Get( "button" ),
            "backgroundPosition" : ( 1280*.50, 550 ),
            #"iconImage" : self.images["button_icons"],
            #"iconPosition" : ( 40, 25 ),
            #"iconBlitRect" : ( 150, 0, 50, 50 ),
            "text" : "About",
            "textPosition" : ( 110, 15 ),
            "textColor" : pygame.Color( 75, 90, 5 ),
            "font" : FontManager.Get( "main" )
            } )
        
        self.buttonItems["quitButton"] = Button()
        self.buttonItems["quitButton"].Setup( { 
            "backgroundImage" : ImageManager.Get( "button" ),
            "backgroundPosition" : ( 1280*.75, 550 ),
            #"iconImage" : self.images["button_icons"],
            #"iconPosition" : ( 40, 25 ),
            #"iconBlitRect" : ( 150, 0, 50, 50 ),
            "text" : "Exit",
            "textPosition" : ( 110, 15 ),
            "textColor" : pygame.Color( 75, 90, 5 ),
            "font" : FontManager.Get( "main" )
            } )

        self.labelItems["credit1"] = Label()
        self.labelItems["credit1"].Setup( {
            "font" : FontManager.Get( "credit" ),
            "text" : "Music from Incompetech, by Kevin MacLeod: Salty Ditty and Cyborg Ninja",
            "position" : ( 10, 720-30 ),
            "color" : pygame.Color( 255, 255, 255 ),
            "style" : "outline",
            "secondary_color" : pygame.Color( 0, 0, 0 )
            } )

        self.labelItems["credit2"] = Label()
        self.labelItems["credit2"].Setup( {
            "font" : FontManager.Get( "credit" ),
            "text" : "Microgame by Rachel Singh",
            "position" : ( 10, 720-60 ),
            "color" : pygame.Color( 255, 255, 255 ),
            "style" : "outline",
            "secondary_color" : pygame.Color( 0, 0, 0 )
            } )

        #SoundManager.PlayMusic( "title" )
        
    def Update( self ):
        
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            
            if ( event.type == MOUSEBUTTONUP ):
                mousex, mousey = event.pos

                if ( self.buttonItems["quitButton"].IsClicked( mousex, mousey ) == True ):
                    pygame.quit()
                    sys.exit()
                    
                elif ( self.buttonItems["playButton"].IsClicked( mousex, mousey ) == True ):
                    self.gotoState = "game"

        self.animateCounter = self.animateCounter + 0.5
        if ( self.animateCounter < 10 ):
            self.zoomCounter = self.zoomCounter + 0.1
            if ( self.zoomCounter > 10 ):
                self.zoomCounter = 10
            
        elif ( self.animateCounter < 20 ):
            self.zoomCounter = self.zoomCounter - 0.1
            if ( self.zoomCounter < 2 ):
                self.zoomCounter = 2
        else:
            self.animateCounter = 0
                


    
    
    def Draw( self, windowSurface ):
        windowSurface.fill( self.backgroundColor )
        
        img = ImageManager.Get( "background" )
        rect = img.get_rect()
        adjWidth = int(rect.width*self.zoomCounter)
        adjHeight = int(rect.height*self.zoomCounter)
        img = pygame.transform.scale( img, (adjWidth, adjHeight ) )
        rect = img.get_rect()
        
        windowSurface.blit( img, ( 1280/2 - adjWidth/2, 720/2 - adjHeight/2 ) )
        
        for image in self.imageItems:
            self.imageItems[image].Draw( windowSurface )
            
        for button in self.buttonItems:
            self.buttonItems[button].Draw( windowSurface )
            
        for label in self.labelItems:
            self.labelItems[label].Draw( windowSurface )
        
        #text1 = FontManager.Get( "win" ).render( "TOILET PAPER: THE GAME", False, pygame.Color( 255, 255, 0 ) )
        #windowSurface.blit( text1, ( 250, 100 ) )

    
