#!/usr/bin/python
# -*- coding: utf-8 -*-

import random
import math

import pygame, sys
from pygame.locals import *

from Ezha import BaseState
from Ezha import FontManager
from Ezha import ImageManager
from Ezha import LanguageManager
from Ezha import SoundManager
from Ezha import Button
from Ezha import Image
from Ezha import Label

from PhysObject import PhysObject

class GameState( BaseState ):
    def __init__( self ):
        self.stateName = "gamestate"

    def GotoState( self ):
        return self.gotoState

    def GetDistance( self, rectA, rectB ):
        dx = rectB.x - rectA.x
        dy = rectB.y - rectA.y
        return math.sqrt( dx ** 2 + dy ** 2 )

    def IsBoxCollision( self, rectA, rectB ):
        return (    rectA.x <= rectB.x + rectB.width and
                    rectA.x + rectA.width >= rectB.x and
                    rectA.y <= rectB.y + rectB.height and
                    rectA.y + rectA.height >= rectB.y )

    def Setup( self, screenWidth, screenHeight ):
        self.screenWidth = screenWidth
        self.screenHeight = screenHeight

        self.backgroundColor = pygame.Color( 151, 255, 140 )
        self.scoreColor = pygame.Color( 255, 255, 255 )

        ImageManager.Add( "background", "assets/images/title-screen.png" )
        ImageManager.Add( "window",     "assets/images/sub-window.png" )
        ImageManager.Add( "handBtn",     "assets/images/hand-button.png" )
        ImageManager.Add( "hands",      "assets/images/player.png" )
        ImageManager.Add( "faces",      "assets/images/faces.png" )
        #ImageManager.Add( "toiletpaper", "assets/fresh-tp.png" )

        self.objects = {}
        self.objects["player"] = PhysObject()
        self.objects["player"].Setup( ImageManager.Get( "hands" ), 0, 0, 177, 920 )
        self.objects["player"].SetPosition( 2000, 2000 )
        self.objects["player"].SetCrop( True )
        
        self.objects["faces"] = {}

        #self.text = FontManager.Get( "main" ).render( self.steps[ self.step ], False, self.scoreColor )

        #SoundManager.PlayMusic( "game" )

        self.gotoState = ""

        self.buttonItems = {}
        self.imageItems = {}
        self.labelItems = {}
        
        self.skinType = 0
        self.currentLevel = 0
        self.substate = ""
        self.levelStartCountdown = 0

        self.SetupSubstate( "char-creation" )

    def SetupSubstate( self, substate ):
        self.buttonItems = {}
        self.imageItems = {}
        self.labelItems = {}
        self.substate = substate
        
        if ( substate == "char-creation" ):
            self.imageItems["char-creation-bg"] = Image()
            self.imageItems["char-creation-bg"].Setup( { 
                "image" : ImageManager.Get( "window" ),
                "imagePosition" : ( 0, 0 ),
                "imageBlitRect" : ( 0, 0, 1280, 720 ),
                "effect" : "bob"
                } )
                
            self.labelItems["instruction"] = Label()
            self.labelItems["instruction"].Setup( {
                "font" : FontManager.Get( "instructions" ),
                "text" : "Select your hand",
                "position" : ( 1280*0.30, 150 ),
                "color" : pygame.Color( 255, 255, 255 ),
                "style" : "outline",
                "secondary_color" : pygame.Color( 0, 0, 0 )
                } )
                
            self.buttonItems["hand1"] = Button()
            self.buttonItems["hand1"].Setup( { 
                "backgroundImage" : ImageManager.Get( "handBtn" ),
                "backgroundPosition" : ( 1280*0.15 + 70, 300 ),
                "iconImage" : ImageManager.Get( "hands" ),
                "iconPosition" : ( 5, 5 ),
                "iconBlitRect" : ( 177*0, 920, 177, 190 )
                } )
                
            self.buttonItems["hand2"] = Button()
            self.buttonItems["hand2"].Setup( { 
                "backgroundImage" : ImageManager.Get( "handBtn" ),
                "backgroundPosition" : ( 1280*0.30 + 70, 300 ),
                "iconImage" : ImageManager.Get( "hands" ),
                "iconPosition" : ( 5, 5 ),
                "iconBlitRect" : ( 177*1, 920, 177, 190 )
                } )
                
            self.buttonItems["hand3"] = Button()
            self.buttonItems["hand3"].Setup( { 
                "backgroundImage" : ImageManager.Get( "handBtn" ),
                "backgroundPosition" : ( 1280*0.45 + 70, 300 ),
                "iconImage" : ImageManager.Get( "hands" ),
                "iconPosition" : ( 5, 5 ),
                "iconBlitRect" : ( 177*2, 920, 177, 190 )
                } )
                
            self.buttonItems["hand4"] = Button()
            self.buttonItems["hand4"].Setup( { 
                "backgroundImage" : ImageManager.Get( "handBtn" ),
                "backgroundPosition" : ( 1280*0.60 + 70, 300 ),
                "iconImage" : ImageManager.Get( "hands" ),
                "iconPosition" : ( 5, 5 ),
                "iconBlitRect" : ( 177*3, 920, 177, 190 )
                } )
            
    def SetupLevel( self, level ):
        # Clear the UI
        self.buttonItems = {}
        self.imageItems = {}
        self.labelItems = {}
        
        self.substate = "game"
        
        self.objects["player"].SetDimensions( 177, 920 )
        self.objects["player"].ChangeFrame( self.skinType * self.objects["player"].posRect.width, 0 )
        
        self.currentLevel = level
        
        self.levelStartCountdown = 100
        
        self.labelItems["level"] = Label()
        self.labelItems["level"].Setup( {
            "font" : FontManager.Get( "instructions" ),
            "text" : "Level " + str( level ),
            "position" : ( 1280/2 - 100, 720/2 - 50 ),
            "color" : pygame.Color( 255, 255, 255 ),
            "style" : "outline",
            "secondary_color" : pygame.Color( 0, 0, 0 )
            } )
        
    def StartLevel( self ):
        # Clear the UI
        self.buttonItems = {}
        self.imageItems = {}
        self.labelItems = {}

        # Setup faces
        self.objects["faces"] = {}
        
        if ( self.currentLevel == 1 ):
            face = PhysObject()
            face.Setup( ImageManager.Get( "faces" ), 0, 0, 301, 186 )
            face.SetPosition( 1280/2 - 301/2, 720/2 - 186/2 )
            face.SetCrop( True )
        
        
    def Update( self ):
        if ( self.levelStartCountdown > 0 ):
            self.levelStartCountdown -= 1
        if ( self.levelStartCountdown == 0 ):
            self.StartLevel()

        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()

            if ( event.type == MOUSEBUTTONUP ):
                mousex, mousey = event.pos
                
                # Setup substate
                if ( self.substate == "char-creation" ):
                    if ( self.buttonItems["hand1"].IsClicked( mousex, mousey ) ):
                        self.skinType = 0
                        self.SetupLevel( 1 )
                    elif ( self.buttonItems["hand2"].IsClicked( mousex, mousey ) ):
                        self.skinType = 1
                        self.SetupLevel( 1 )
                    elif ( self.buttonItems["hand3"].IsClicked( mousex, mousey ) ):
                        self.skinType = 2
                        self.SetupLevel( 1 )
                    elif ( self.buttonItems["hand4"].IsClicked( mousex, mousey ) ):
                        self.skinType = 3
                        self.SetupLevel( 1 )

            elif ( event.type == MOUSEBUTTONDOWN ):
                pass
                #for key, obj in self.objects.iteritems():

            elif ( event.type == MOUSEMOTION ):
                mousex, mousey = event.pos
                
                if ( self.substate == "game" and self.levelStartCountdown <= 0 ):
                    self.objects["player"].SetPosition( mousex - 70, mousey - 100 )
                    
                #self.hand.SetPosition( mousex - 70, mousey - 60 )

                #for key, obj in self.objects.iteritems():
                #    if ( self.holding == key and obj.GetCanBeMoved() == True ):
                #        obj.SetPosition( mousex - 70, mousey - 60 )


            if ( event.type == KEYDOWN ):
                if ( event.key == K_d ):
                    pass
                    #print( "TP angle:", self.objects["tp"].GetAngle() )
                    #print( "TP pos:", self.objects["tp"].posRect )
                    #print( "Holder pos:", self.objects["tpholder"].posRect )
                    #print( "Collision?", self.IsBoxCollision( self.objects["tp"].posRect, self.objects["tpholder"].posRect ) )

                elif ( event.key == K_ESCAPE ):
                    self.gotoState = "title"

        #for key, obj in self.objects.iteritems():
            #obj.Update()



    def Draw( self, windowSurface ):
        windowSurface.fill( self.backgroundColor )
        windowSurface.blit( ImageManager.Get( "background" ), ( 0, 0 ) )

        for image in self.imageItems:
            self.imageItems[image].Draw( windowSurface )
            
        for button in self.buttonItems:
            self.buttonItems[button].Draw( windowSurface )
            
        for label in self.labelItems:
            self.labelItems[label].Draw( windowSurface )
            
            
        #windowSurface.blit( ImageManager.Get( "basketb" ), ( 224, 548 ) )

        self.objects["player"].Draw( windowSurface )

        #windowSurface.blit( ImageManager.Get( "basketf" ), ( 224, 548 ) )

        # self.hand.Draw( windowSurface )

        # if ( self.step < 4 ):
            # windowSurface.blit( self.text, ( 10, 10 ) )
        # elif ( self.step == 4 ):
            # x = 300
            # y = 720/2 - 60
            # self.jitter = random.randint( -2, 2 )

            # text1 = FontManager.Get( "win" ).render( "OH MY GOD, YOU DID IT!", False, pygame.Color( 255, 255, 0 ) )
            # text2 = FontManager.Get( "win" ).render( "I THOUGHT IT WAS IMPOSSIBLE!!!", False, pygame.Color( 255, 0, 0 ) )
            # windowSurface.blit( text1, ( x + self.jitter, y +  self.jitter ) )
            # windowSurface.blit( text2, ( x - 100 + self.jitter, y + 60 + self.jitter ) )

