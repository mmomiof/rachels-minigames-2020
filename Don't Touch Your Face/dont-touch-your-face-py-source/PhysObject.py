#!/usr/bin/python
# -*- coding: utf-8 -*-

import random
import math

from Ezha import Character 

import pygame, sys
from pygame.locals import *

class PhysObject( Character ):
    def __init__( self ):
        self.angle = 0
        self.xVel = 0
        self.yVel = 0
        self.maxVel = 0
        self.acc = 2
        self.deacc = 0.2
        self.crop = False
        self.canBeMoved = False
        self.inactive = False

    def SetInactive( self, val ):
        self.inactive = val

    def GetIsInactive( self ):
        return self.inactive

    def SetCrop( self, val ):
        self.crop = val

    def SetCanBeMoved( self, val ):
        self.canBeMoved = val

    def GetCanBeMoved( self ):
        return self.canBeMoved

    def RotateImage( self, image, rect, amount ): # https://www.pygame.org/wiki/RotateCenter
        rotateImage = pygame.transform.rotate( image, amount )
        rotateRect = rotateImage.get_rect( center=rect.center )
        return rotateImage, rotateRect

    def Rotate( self, amount ):
        self.angle = self.angle + amount
        if ( self.angle >= 360 ):
            self.angle = 0

    def GetAngle( self ):
        return self.angle

    def Update( self ):
        if ( self.canBeMoved == False ):
            return
        self.MoveXY( self.xVel, self.yVel )

        # Bounce
        if ( self.posRect.x < 0 or self.posRect.x + self.posRect.width > 1280 ):
            self.xVel = -self.xVel

        if ( self.posRect.y < 0 or self.posRect.y + self.posRect.height > 720 ):
            self.yVel = -self.yVel

        if ( self.posRect.x < 0 ):
            self.posRect.x = 0
            
        elif ( self.posRect.x + self.posRect.width > 1280 ):
            self.posRect.x = 1280 - self.posRect.width

        if ( self.posRect.y < 0 ):
            self.posRect.y = 0
            
        elif ( self.posRect.y + self.posRect.height > 720 ):
            self.posRect.y = 720 - self.posRect.height

        self.Deaccelerate()

    def SetMaxAcceleration( self, maximum ):
        self.maxVel = maximum

    def AccelerateXY( self, x, y ):
        self.xVel = self.xVel + x
        self.yVel = self.yVel + y

        if ( self.xVel > self.maxVel ):
            self.xVel = self.maxVel
        elif ( self.xVel < -self.maxVel ):
            self.xVel = -self.maxVel

        if ( self.yVel > self.maxVel ):
            self.yVel = self.maxVel
        elif ( self.yVel < -self.maxVel ):
            self.yVel = -self.maxVel

    def Deaccelerate( self ):
        if ( self.xVel < 0 ):
            self.xVel = self.xVel + self.deacc
            
        elif ( self.xVel > 0 ):
            self.xVel = self.xVel - self.deacc
            
        if ( self.yVel < 0 ):
            self.yVel = self.yVel + self.deacc
            
        elif ( self.yVel > 0 ):
            self.yVel = self.yVel - self.deacc

    def SetVelocity( self, x, y ):
        self.xVel = 0
        self.yVel = 0

    def Accelerate( self, direction ):
        if ( direction == "UP" ):
            self.AccelerateXY( 0, -self.acc )
            
        elif ( direction == "DOWN" ):
            self.AccelerateXY( 0, self.acc )
            
        elif ( direction == "LEFT" ):
            self.AccelerateXY( -self.acc, 0 )
            
        elif ( direction == "RIGHT" ):
            self.AccelerateXY( self.acc, 0 )

    def Draw( self, window ):
        if ( self.inactive == True ):
            return
            
        if ( self.image is not None ):
            if ( self.crop ):
                window.blit( self.image, self.posRect, self.frameRect )
                
            else:
                if ( self.angle == 0 ):
                    window.blit( self.image, self.posRect )
                else:
                    
                    image, rect = self.RotateImage( self.image, self.image.get_rect(), self.angle )

                    window.blit( image, self.posRect )
