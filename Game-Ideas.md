# Game Ideas

**Goals:**

* Make something smallish that can be contained within about a month.
* Work on a project that I find personally engaging (generally means there's a message or a lesson in it.)

**Rules:**

* Game ideas should be "game jam" sized games; I shouldn't be over-scoping.
* Think of these games as a "demo" or "prototype", which could be expanded on later.

# List of game ideas

## Data Structures

![Example sketch](illustrations/datastructures.png)

In this simple game, you learn basic functionaliy of core data structures by playing with blocks.

For example, in a list, if you want to "insert" an item in the middle of the list,
the player has to physically move the tail block further down,
and each block to its left up until the insert index,
and then they can put the new value in at that position.
(Basically doing a shift right, showing that you'd need to start
a shift-right at the END of the list, shifting everything forward.)

The idea is to give students a visual reference for how these data structures work
and how functions like push, pop, insert, etc. work.

**Prototype:** Maybe this would only have one data structure and a couple of functions as an example.

## Don't Touch Your Face

![Example sketch](illustrations/donttouchyourface.png)

A simple game in the same vein as [Toilet Paper](https://moosader.itch.io/toilet-paper)
where the goal is to NOT TOUCH YOUR FACE. Your face will be represented on the screen,
and you control your hand, and you have to navigate levels to avoid your face.
For example, the hand might continually gravitate towards your face,
and you must keep moving it away. Or maybe the face is moving around and you have to give it space.

(Maybe kind of SHMUP-like??)

**Prototype:** Mostly revolves around player control and face. No/little time spent on scenery.

## Pupreciation

![Example sketch](illustrations/pupreciation.png)

A game that is like a Tinder-style interface, but you just look at dogs and rate them "good" (right) or "bad" (left).
No internet connectivity or actual purpose, just so you can look at dogs on your phone instead of browsing social media.

(I mean maybe I could connect it to the internet to track dog stats to see who is the #1 good boy)

**Prototype:** Only feature is swiping left/right (or have the x and heart buttons), loading and showing dog pictures, 
and maybe keeping a dog leaderboard.

